//
//  Constants.swift
//  Golden Gym
//
//  Created by Hostienda_Movil on 4/5/18.
//  Copyright © 2018 sandro_test. All rights reserved.
//

import Foundation
import UIKit

var defaults = UserDefaults.standard

func DialogAlerConections(Titulo: String, Mensaje: String, Vista: UIViewController){
    //Alerta
    let alert = UIAlertController(title: Titulo, message: Mensaje, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
    Vista.present(alert, animated: true, completion: nil)
}
