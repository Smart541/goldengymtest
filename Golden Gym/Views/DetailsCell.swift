//
//  DetailsCell.swift
//  Golden Gym
//
//  Created by Hostienda_Movil on 4/5/18.
//  Copyright © 2018 sandro_test. All rights reserved.
//

import UIKit

class DetailsCell: UITableViewCell {
    
    @IBOutlet weak var lbl_weight: UILabel!
    @IBOutlet weak var lbl_height: UILabel!
    @IBOutlet weak var lbl_imc: UILabel!
    @IBOutlet weak var lbl_gender: UILabel!
    @IBOutlet weak var lbl_age: UILabel!
    @IBOutlet weak var lbl_date: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
