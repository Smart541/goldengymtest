//
//  CalculationsController.swift
//  Golden Gym
//
//  Created by Hostienda_Movil on 4/5/18.
//  Copyright © 2018 sandro_test. All rights reserved.
//

import UIKit
import CoreData

class CalculationsController: UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var tf_age: UITextField!
    @IBOutlet weak var tf_height: UITextField!
    @IBOutlet weak var tf_weight: UITextField!
    @IBOutlet weak var tf_gender: UITextField!
    @IBOutlet weak var lbl_results: UILabel!
    
    let pickerView = UIPickerView()
    let genderArray = ["Masculino","Femenino"]
    var results = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pickerView.delegate = self
        self.tf_gender.inputView = pickerView
        self.tf_weight.delegate = self
        self.tf_height.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func calculate(_ sender: Any) {
        if filledInputs(){
            self.setClasification(IMC: calcularIMC())
        }else{
            DialogAlerConections(Titulo: "ATENCION", Mensaje: "Alguno de los campos estan vacios", Vista: self)
        }
    }
    
    func filledInputs() -> Bool{
        if tf_age.text != "" && tf_weight.text != "" && tf_height.text != "" && tf_gender.text != "" {
            return true
        }
        return false
    }
    
    func calcularIMC() -> Float{
        if let pesoStr = tf_weight.text,
            let pesoDouble = Float(pesoStr),
            let alturaStr = tf_height.text,
            let alturaDouble = Float(alturaStr){
            let IMC = Float(pesoDouble / (alturaDouble * alturaDouble))
            return IMC
        }
        return -1
    }
    
    func setClasification(IMC : Float){
        switch IMC {
        case 0...18 :
            self.lbl_results.text = "Peso bajo. Necesario valorar signos de desnutrición"
            break
            
        case 18...24.9 :
            self.lbl_results.text = "Normal"
            break
            
        case 25...26.9 :
            self.lbl_results.text = "Obesidad"
            break
            
        case 27...29 :
            self.lbl_results.text = "Obesidad grado I. Riesgo relativo alto para desarrollar enfermedades cardiovasculares"
            break
            
        case 30...39.9 :
            self.lbl_results.text = "Obesidad grado II. Riesgo relativo muy alto para desarrollar enfermedades cardiovasculares"
            break
            
        case let IMC where IMC >= 40 :
            self.lbl_results.text = "Obesidad grado III Extrema o Mórbida. Riesgo relativo muy alto para desarrollar enfermedades cardiovasculares"
            break
            
        default: self.lbl_results.text = "Error"
            break
        }
        
        if let sexo = self.tf_gender.text {
            saveData(sexo: sexo, imc: IMC)
        }
        
    }
    
    func saveData(sexo : String, imc: Float){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "User_History", in: managedContext)
        let data = NSManagedObject(entity: entity!, insertInto: managedContext)
        data.setValue(Int(tf_age.text!), forKey: "age")
        data.setValue(getToday(), forKey: "date")
        data.setValue(sexo, forKey: "gender")
        data.setValue(Float(tf_height.text!), forKey: "height")
        data.setValue(Float(tf_weight.text!), forKey: "weight")
        data.setValue(imc, forKey: "imc")
        do {
            try managedContext.save()
            results.append(data)
        } catch let error as NSError {
            print("No ha sido posible guardar \(error), \(error.userInfo)")
        }
    }
    
    func getToday () -> Date{
        let date = Date()
        return date.format()
    }
    
    @IBAction func showList(_ sender: Any) {
        performSegue(withIdentifier: "showList", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tf_gender.text = genderArray[row]
        self.view.endEditing(true)
    }
    
}

extension Date {
    
    func format(format:String = "dd/MM/yyyy") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let dateString = dateFormatter.string(from: self)
        if let newDate = dateFormatter.date(from: dateString) {
            return newDate
        } else {
            return self
        }
    }
}
