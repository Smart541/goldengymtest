//
//  ViewController.swift
//  Golden Gym
//
//  Created by Hostienda_Movil on 4/5/18.
//  Copyright © 2018 sandro_test. All rights reserved.
//

import UIKit
import CoreData

class ListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var results = [NSManagedObject]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let request = NSFetchRequest <NSFetchRequestResult>(entityName: "User_History")
        let sort = NSSortDescriptor(key: #keyPath(User_History.date), ascending : true)
        request.sortDescriptors = [sort]
        let context = appDelegate.persistentContainer.viewContext
        request.returnsObjectsAsFaults = false
        do{
            let result = try context.fetch(request)
            results = result as! [NSManagedObject]
        }catch{
            print("FALLO")
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DetailsCell
        let item = results[indexPath.row]
        
        if let weight = item.value(forKey: "weight") as? Float{
            cell.lbl_weight.text = "\(weight)"
        }
        if let height = item.value(forKey: "height") as? Float{
            cell.lbl_height.text = "\(height)"
        }
        if let imc = item.value(forKey: "imc") as? Float{
            cell.lbl_imc.text = "\(imc)"
        }
        if let age = item.value(forKey: "age") as? Int{
            cell.lbl_age.text = "\(age)"
        }
        if let gender = item.value(forKey: "gender") as? String{
            cell.lbl_gender.text = "\(gender)"
        }
        if let date = item.value(forKey: "date") as? Date{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            cell.lbl_date.text = "\(formatter.string(from: date))"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .default, title: "Eliminar", handler: { (action, indexPath) in
            // remove the deleted item from the model
            let context = self.appDelegate.persistentContainer.viewContext
            context.delete(self.results[indexPath.row] as NSManagedObject)
            self.results.remove(at: indexPath.row)
            do{
                try context.save()
            }catch{
                print("FALLO")
            }
            // remove the deleted item from the `UITableView`
            self.tableView.deleteRows(at: [indexPath], with: .fade)
        })
        deleteAction.backgroundColor = UIColor.red
        return [deleteAction]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
